<?php namespace App\Http\Controllers;
 
 use App\User;
 use App\Http\Controllers\Controller;
 use App\Http\Requests;
 use Illuminate\Support\Facades\Validator;
 use Illuminate\Support\Facades\Redirect;
 use Illuminate\Http\Request;
 use App\Music_chart;
date_default_timezone_set("Asia/Ho_Chi_Minh");
class AddController extends Controller {

	public function addsong(Request $request) {	
	    if($request->ajax()) {    	
	    	$song_id= $request->get('song_id');
	    	$image=$request->get('image');
	    	$title= $request->get('title');
	    	$artist= $request->get('artist'); 
	    	$linkplay= $request->get('linkplay');
	    	$data=$this->handle($song_id,$image,$title,$artist,$linkplay);

	    	return "$data";
		}else{
			return "AddController";
		}
	}
	private function handle($song_id,$image,$title,$artist,$linkplay) {
		$check_exist_song=Music_chart::where('song_id','=',$song_id)->get();
		if($check_exist_song->count()< 1){
			$check_number_song = Music_chart::orderBy('chart', 'DSC')->get();
			if($check_number_song->count()<1){
				$chart=0;
			}else{
				$chart=Music_chart::orderBy('chart','DSC')->first()->chart;
			}
			$new_chart = $chart +1;
			$new_song = new Music_chart;
			$new_song->song_id=$song_id;
			$new_song->image=$image;
			$new_song->title=$title;
			$new_song->artist=$artist;
			$new_song->linkplay=$linkplay;
			$new_song->chart=$new_chart;
			$new_song->inlist=1;
			$new_song->save();

				
		}else{
			$chart=Music_chart::orderBy('chart','DSC')->first()->chart;
			$new_chart=$chart +1;
			Music_chart::where('song_id','=',$song_id)->update(array('inlist' => 1,'chart'=> $new_chart));
		}
			$music = Music_chart::where('chart','>','0')->orderBy('chart','ASC')->get();
			$data='';
			$id=0;
	    	foreach ($music as $a) {
	    		$data=$data."
	    		<div class='col-lg-12 list1'>
                  <div class='col-lg-2 khai-play' id='$a->id'><h2 class='form'>$a->chart <img class='avata' src='$a->image' alt='Flower' width='50px' height='50px'>
                </h2></div>
                <div class='col-lg-5 khai-play' id='$a->id'><p>
                   <table class=‘tablelist’>
                  <tr>
                    <th>$a->title</th>
                  </tr>
                   <tr>
                    <td>$a->artist</td>
                  </tr>
                </table></p>
                </div>
                   <div class='col-lg-1 col-lg-offset-2'><p>
                    <table class='ftb'>
                      <tr>
                        <th class='up'>
                          <a class='iconvote' id='$a->id' name='up' data-toggle='tooltip' data-placement='bottom' title='vote  up '><img src='image/up.png' width='40px' height='40px'></a>
                        </th>
                        <th>
                          <a class='iconvote' id='$a->id' name='down' data-toggle='tooltip' data-placement='bottom' title='vote down'><img src='image/down.png' width='40px' height='40px'></a>
                        </th>
                      </tr>
                     </table></p>
                                 
                </div>
                </div>
                <div id='k".$a->id."' class='khai-feedback'></div>";
                $id=$a->id;
	    	};

$member = array('list' => $data
                   ,'idt' => $id
                  );
      return (json_encode($member));
	}
}

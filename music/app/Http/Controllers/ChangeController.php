<?php namespace App\Http\Controllers;

 use App\User;
 use App\Http\Controllers\Controller;
 use App\Http\Requests;
 use Illuminate\Http\Request;
 use Illuminate\Support\Facades\Validator;
 use Illuminate\Support\Facades\Redirect;
 use Illuminate\Http\RedirectResponse ;
 use Illuminate\Routing\Redirector;
 use Session;
 use Auth;


class ChangeController extends Controller {

	public function getchange(){
		return view('changeinformal');
	}

	public function getinfor(){
		$users = new User;
		$user = $users->getinforbyname(session('user'));
		$result = array($user->name,$user->email);  
		return $result;
	}

	public function changename(Request $input){
		$users = new User;
		$vali = Validator::make($input->all(), [
            'name' => 'required|min:4|unique:users',
            ]);
		if($vali->passes()){
			$users->updatename(session('user'),$input->name);
			if($input->name!=""){
				\Session::forget('user');
				session(["user"=>$input->name]);
				return  $input->name;
			}	
		}
		else return;
	}

	public function changeemail(Request $input){
		$users = new User;
		$vali = Validator::make($input->all(), [
            'email'=> 'required|email|unique:users',
            ]);
		if($vali->passes()){
			$users->updateemail(session('user'),$input->email);
			return $input->email;
		}
		else return;		
	}

	public function changepass(Request $input){
		$users = new User;
		$vali = Validator::make($input->all(), [
            'password'=> 'required|min:6',
            'pass_conf'=> 'required|min:6',
            ]);

		if($vali->passes() && $input->password == $input->pass_conf && $users->checkpassbyname(session('user'),md5($input->currentpass)) == true){
			$users->updatepass(session('user'),$input->password);
			return 'success';
		}
		else return;
	}

	public function checkemail(Request $input){
		$user = new User;
		$check = $user->checkemailbyname(session('user'),$input->email);
		if($check==true)
			return 'false';
		return 'true';

	}

	public function checkname(Request $input){
		$user = new User;
		$check = $user->checknamebyname(session('user'),$input->name);
		if($check==true)
			return 'false';
		return 'true';
	}

	public function checkpass(Request $input){
		$user = new User;
		$check = $user->checkpassbyname(session('user'),md5($input->curpassword));
		if(!$check)
			return 'false';
		return 'true';
	}
}
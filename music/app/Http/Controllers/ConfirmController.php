<?php namespace App\Http\Controllers;

 use App\User;
 use App\Http\Controllers\Controller;
 use App\Http\Requests;
 use Illuminate\Http\Request;
 use Illuminate\Support\Facades\Validator;
 use Illuminate\Support\Facades\Redirect;
 use Illuminate\Http\RedirectResponse ;
 use Illuminate\Routing\Redirector;
 use Session;
 use Auth;

class ConfirmController extends Controller {

    public function getconfirm(){
        return view('confirm');
    }

    public function postconfirm(){
        $user = new User;

        $code = \Input::get('codeconf');
        $email = session('email');

		if($user->checkconfirm_code($email, $code)){
			$user->where('email','=',$email)
			     ->update(["check"=>"yes"]);
			     
			$username = $user->where('email','=',$email)->first()->name;
			session(["user"=>$username]);

			return "success";
		}
		else{
			return '<label class="alert alert-danger btn-block" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    Please review code confirm
                    </label>';
		}
    }

}
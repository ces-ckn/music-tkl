<?php namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\User;
use Session;

class ForgotpassController extends Controller {
	public function getforgot(){
		return view('forgotpass');
	}

	public function checkalready(Request $input){
		$user = new User;
        if(!$user->checkemail($input->get('email')))
        	return 'false';
        return 'true';
	}

	public function postforgot(Request $input){
		$user = new User;
		Mail::send('mailforgot', array("user"=>$user), function($message) use ($input){
        $message->to($input->get('email'),"Remember")
            ->subject('Laravel 5 Forgot Password');
        });

        $email = $input->get('email');
        session(["email"=>$email]);
        
        return "<div class='alert alert-success'>
  				<strong>Success!</strong> Completed request. Please, check in E-mail.
				</div>";
	}

	public function getupdatepassword(){
		return view("getpass");
	}

	public function postupdatepassword(Request $input){
		$user = new User;
        $email = session('email');
        $pass = md5($input->get('password'));

		$user->where('email','=',$email)
		     ->update(["password"=>$pass]);
		     
		$username = $user->where('email','=',$email)->first()->name;
		if($user->checkactivation($email))
		   session(["user"=>$username]);

		return redirect('home');
	}
}
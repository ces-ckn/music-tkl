<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Routing\Redirector;
use Session;

class HomeController extends Controller {
	
	public function index(){
		return view('index');
	}
}

<?php namespace App\Http\Controllers;

 use App\User;
 use App\Http\Controllers\Controller;
 use App\Http\Requests;
 use Illuminate\Http\Request;
 use Illuminate\Support\Facades\Validator;
 use Illuminate\Support\Facades\Redirect;
 use Illuminate\Http\RedirectResponse ;
 use Illuminate\Routing\Redirector;
 use Session;
 use Auth;

class LoginController extends Controller {

    public function postlogin(Request $input){

        $user = new User;
        $password=md5($input->get('password'));
        $email=$input->get('email');
        $ce = array('email'=>$email, 'password'=>$password);
        
        if($user->checklogin($email, $password)){
            $check=$user->checkactivation($email);
            if($check=='yes'){
                if($user->checkroles($email)==1)
                    session(["admin"=>$user->getNamelogin($email)]);
                else if($user->checkroles($email)==2)
                    session(["user"=>$user->getNamelogin($email)]);
                else 
                    session(["slave"=>$user->getNamelogin($email)]);
                return "login";
            }
            else if($check=='no'){
                session(["email"=>$email]);
                return "confirm";
            }
            else{
                return '<label class="alert alert-danger btn-block" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    Account have been blocked
                    </label>';
            }  
        }else{
            return '<label class="alert alert-danger btn-block" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    Please review email or password
                    </label>';
        }
    }

    public function postloginface(){
        if(\Input::has('code'))
        {

            $faceuser = \Socialize::with('facebook')->user();

            $user = new User;

            if(!$user->checksocial_id($faceuser->getId())){
                $user->name=$faceuser->getName();
                $user->Social_id=$faceuser->getId();
                $user->level = 2;
                $user->save();
            }

            session(["user"=>$faceuser->getName()]);

            return redirect('home');
                
        }
        else
        {
            return \Socialize::with('facebook')->redirect();;
        }
    }

    public function postlogingoogle(){
        if(\Input::has('state'))
        {
            $googleuser = \Socialize::with('google')->user();

            $user = new User;

            if(!$user->checksocial_id($googleuser->getId())){
                $user->name=$googleuser->getName();
                $user->Social_id=$googleuser->getId();
                $user->level = 2;
                $user->save();
            }

            session(["user"=>$googleuser->getName()]);

            return redirect('home');

        }
        else
        {
            return \Socialize::with('google')->redirect();;
        }
    }

    public function logout(){

        if (Session::has('user'))
            Session::forget('user');
        else if(Session::has('admin'))
            Session::forget('admin');
        else
            Session::forget('slave');
        
        return redirect('/');
    }
}
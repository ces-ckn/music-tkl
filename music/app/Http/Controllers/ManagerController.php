<?php namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\User;
use Session;

class ManagerController extends Controller {

	public function getinforaccount(){
		if(Session::has('admin')){
			$user = new User;
			$users = $user->getinfor();
			return view('usermanager', compact('users'));
		}
	}

	public function blockaccount(Request $input){
		$id = $input->get('ids');
		$user = new User;
		$check = $user->updatecheck($id);
		return $check;
	}
}
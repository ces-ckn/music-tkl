<?php namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\User;
use Session;

class RegisterControlller extends Controller {

    public function postregister(Request $input){

		$confirmation_code = str_random(6);
		$user = new User;
        
		$vali = Validator::make($input->all(), [
            'name' => 'required|min:4|unique:users',
            'email'=> 'required|email|unique:users',
            'password'=> 'required|min:6',
            'password_confirmation' => 'required|min:6'
            ]);

        if($vali->passes() && $input->get('password') == $input->get('password_confirmation')){
            $user->name = $input->get('name');
            $user->password = md5($input->get('password'));
            $user->email = $input->get('email');
            $user->confirm_code = $confirmation_code;
            $user->check = "no";
            $user->level = 2;
            $user->save();

            Mail::send('mailer', array("user"=>$user), function($message) use ($input){
            $message->to($input->get('email'), $input->get('name'))
                ->subject('Laravel 5 Confirm');
            });

            $email = $input->get('email');
            session(["email"=>$email]);
            return 'success';
        }
        else
            return 'error';
	}

	public function checkemailempty(Request $input){
        $user = new User;
        if($user->checkemail($input->get('email')))
        	return 'false';
        return 'true';
    }

    public function checknameempty(Request $input){
    	$user = new User;
        if($user->checkusername($input->get('name')))
        	return 'false';
        return 'true';
    }
}
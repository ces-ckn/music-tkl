<?php namespace App\Http\Controllers;
 
 use App\User;
 use App\Http\Controllers\Controller;
 use App\Http\Requests;
 use Illuminate\Support\Facades\Validator;
 use Illuminate\Support\Facades\Redirect;
 use Illuminate\Http\Request;
 use App\Music_chart;

class SearchController extends Controller {
/*	public function zingmobile(Request $request) {	
	    if($request->ajax()) {
	    	$keyword= urlencode($request->get('search'));
	    	$length="7";
			$start="0";
			$url='http://api.mp3.zing.vn/api/mobile/search/song?requestdata={"length":'.$length.',"start":'.$start.',"q":"'.$keyword.'","sort":"hot"}&keycode=b319bd16be6d049fdb66c0752298ca30&fromvn=true';
			$data= file_get_contents($url);	
			$result = json_decode($data);
			return json_encode($result->docs);
	    
		}
	}*/
	
	public function searchform(){

		return view('music.searchform');
	}
	public function search(Request $request) {	
	    if($request->ajax()) {
	    	$keyword= urlencode($request->get('search'));
	    	$length=20;
	    	$data=$this->getZingResult($keyword,$length);
	    	return $data;
		}
	}
	private function getZingResult($keyword, $numOfRow) {
		$publicKey="c1e9642938bee8f03f489b09455ba759950f5f9c";
		$privateKey='30ad0ae0e22ddc1736698bfb4bee9258'; 

		$data = array(
				'kw'=>$keyword, 
				't'=>"1", 
				'rc'=> $numOfRow, 
				'p'=>1
				);

		$jsondata =  urlencode(base64_encode(json_encode($data)));
		$signature=hash_hmac('md5',$jsondata , $privateKey);
		
		$apiURL="http://api.mp3.zing.vn/api/search?publicKey=".$publicKey."&signature=".$signature."&jsondata=".$jsondata;
		/*return $apiURL;*/
		$jsonResult = file_get_contents($apiURL);
		$result = json_decode($jsonResult);
		return json_encode($result->Data);
	}
	public function getid(Request $request){
		if($request->ajax()) {
	    	$data=$this->conndata();
	    	return $data;
		}else{
			return "false";
		}
	}
	private function conndata(){
		$check_exist_song=Music_chart::select('song_id')->where('inlist','=','1')->get();
		$idv=Music_chart::select('id')->where('inlist','=','1')->get();
		$member = array('list' => $check_exist_song
                   ,'idv' => $idv
                  );
      
		if($check_exist_song->count() <1){
			return "none-object";
		}else{
			return (json_encode($member));
		}
	}
	
}

<?php namespace App\Http\Controllers;

 use App\Time_play;
 use App\Http\Controllers\Controller;
 use App\Http\Requests;
 use Illuminate\Http\Request;
 use Illuminate\Support\Facades\Validator;
 use Illuminate\Support\Facades\Redirect;
 use Illuminate\Http\RedirectResponse ;
 use Illuminate\Routing\Redirector;
 use Session;
 use Auth;
date_default_timezone_set("Asia/Ho_Chi_Minh");

class TimeController extends Controller {

	public function gettime(){
		$time = new Time_play;
		$data=$time->getalltime();
		/*$end=$time->get_endtime();
		return json_encode(array('alltime' => $data,'end' => $end));*/
		return json_encode(array('alltime' => $data));
	}
	
	function savetime(Request $request){
		if($request->ajax()) {
			$part=$request->get('part');
			$idtime=$request->get('idtime');
			$chart=$request->get('chart');
	    	$start= $request->get('start');
	    	$songs=$request->get('songs'); 	
	    	$act=$request->get('act');
	    	$end=$request->get('end');
	    	$choice=$request->get('choice');
	    	//$this->save($idtime,$chart,$start,$songs,$act);
	    	$this->save($part,$idtime,$chart,$start,$songs,$act,$end,$choice);
		}else{
			return "No Ajax";
		}
	}
	private function save($part,$idtime,$chart,$start,$songs,$act,$end,$choice){
	//private function save($idtime,$chart,$start,$songs,$act){
		$check_exist_time=Time_play::where('id_time','=',$idtime)->get();
		if($check_exist_time->count() < 1){
			$new_time = new Time_play;
			$new_time->id_time=$idtime;
			$new_time->chart=$chart;
			$new_time->time_start=$start;
			$new_time->songs=$songs;
			$new_time->active=$act;
			$new_time->part=$part;
			$new_time->time_end=$end;
			$new_time->choice=$choice;
			$new_time->save();
		}else{
			Time_play::where('id_time','=',$idtime)->update(array('time_start'=> $start,'chart'=>$chart,'songs' => $songs,'time_end' => $end,'active' => $act,'choice'=>$choice));
		}
		
	}
	function deletetime(Request $request){
		if($request->ajax()) {
			$idtime=$request->idtime;
			$chart=$request->chart;
			$this->delete($idtime,$chart);
		}else{
			return "no ajax";
		}
	}
	private function delete($idtime,$chart){
		Time_play::where('id_time','=',$idtime)->delete();
		Time_play::where('chart','>',$chart)->decrement('chart',1);
	}
	function getcurrenttime(){
   		$time=date("H:i:s");
    	return $time;
	}
	function getchart(Request $request){
		if($request->ajax()) {
			$data=$this->chart();
		}
		return $data;
	}
	private function chart(){
		$data = Time_play::orderBy('chart', 'ASC')->get();
		return json_encode($data);;
	}
	function arrangechart(Request $request){
		if($request->ajax()){
			$idtime=$request->idtime;
			$act=$request->act;
			$chart=$request->chart;
			$separation = $request->diff;
			$data = $this->arrange($idtime,$act,$chart,$separation);
		}
	}
	private function arrange($idtime,$act,$chart,$separation){
		if($act==0){
			Time_play::where('chart','>',$chart)->decrement('chart',1);
			$temp = Time_play::select('id_time')->get();
			$all = $temp->count();
			$all = $all-1;
			Time_play::where('id_time','=',$idtime)->update(array('chart'=>$all,'active'=>$act));
		}else{
			Time_play::where('chart','>=',$separation)->increment('chart',1);
			Time_play::where('id_time','=',$idtime)->update(array('chart'=>$separation,'active'=>$act));
			Time_play::where('chart','>',$chart)->decrement('chart',1);
		}
		
	}
	function changetime(Request $request){
		if($request->ajax()){
			$part=$request->part;
			$chart=$request->chart;
			$Time = new Time_play;
			$Time->update_time($part,$chart);
		}else{
			return "No ajax here";
		}
	}
}

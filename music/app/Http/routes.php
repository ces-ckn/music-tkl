<?php

Route::get('/', 'IndexController@index');

Route::post('loginhandle', 'LoginController@postlogin');

Route::get('loginfacehandle', 'LoginController@postloginface');

Route::get('logingooglehandle', 'LoginController@postlogingoogle');

Route::post('registerhandle', 'RegisterControlller@postregister');

Route::get('confirm', 'ConfirmController@getconfirm');

Route::post('confirmhandle', 'ConfirmController@postconfirm');

Route::get('checkusername', 'RegisterControlller@checknameempty');

Route::get('checkemail', 'RegisterControlller@checkemailempty');

Route::get('logout', 'LoginController@logout');

Route::get('test', 'MusicController@test');

Route::get('home', 'IndexController@index');

Route::post('index', 'IndexController@vote');

Route::post('check', 'IndexController@check');

Route::post('/search','SearchController@search');

Route::post('/add','AddController@addsong');

Route::post('/getid','SearchController@getid');

Route::post('/time','TimeController@gettime');

Route::post('/savetime','TimeController@savetime');

Route::post('/getchart','TimeController@getchart');

Route::post('/arrangechart','TimeController@arrangechart');

Route::post('/changetime','TimeController@changetime');

Route::post('/getcurrenttime','TimeController@getcurrenttime');

Route::post('/deletetime','TimeController@deletetime');

Route::post('forgotpasshandle','ForgotpassController@postforgot');

Route::get('forgotpass','ForgotpassController@getforgot');

Route::get('checkalready', 'ForgotpassController@checkalready');

Route::get('getpassword', 'ForgotpassController@getupdatepassword');

Route::post('getpasswordhandle', 'ForgotpassController@postupdatepassword');

Route::get('getchange', 'ChangeController@getchange');

Route::post('getinfor', 'ChangeController@getinfor');

Route::get('checknamechange', 'ChangeController@checkname');

Route::get('checkmailchange', 'ChangeController@checkemail');

Route::get('checkcurrentpass', 'ChangeController@checkpass');

Route::post('changename', 'ChangeController@changename');

Route::post('changeemail', 'ChangeController@changeemail');

Route::post('changepass', 'ChangeController@changepass');

Route::get('play', 'MusicController@play');


Route::post('play','MusicController@playnext');
Route::post('changelist','MusicController@changelist');
Route::post('playrelax','MusicController@playrelax');
Route::get('manaaccount','IndexController@manaaccount' );

Route::get('inforaccount', 'ManagerController@getinforaccount');

Route::post('blockaccount', 'ManagerController@blockaccount');

Route::get('slave', 'IndexController@slavepl');

Route::get('alert', function(){
	return view('alert');
});
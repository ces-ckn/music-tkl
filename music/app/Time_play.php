<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Time_play extends Model {

	protected $table = 'time_plays';
	function update_time($part,$chart){
		Time_play::where('chart','<=',$chart)->decrement('chart',1);
		Time_play::where('part','=',$part)->update(array('chart' => $chart ));
	}
	/*function get_endtime(){
		$end = Time_play::where('id','=',0)->get();
		return $end;
	}*/
	function getalltime(){
		$time = Time_play::where('id','>',0)->orderBy('chart', 'ASC')->get();
		return $time;	
	}

}

<?php namespace App;
use Session; 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class User extends Model {
	protected $table = 'users';

	public function checklogin($email, $password){
		$check=User::where("email","=",$email)->where("password","=",$password)->count();
		if($check>0)
			return true;
		else
			return false;
	}

	public function checkroles($email){
		$user=User::where("email","=",$email)->first();
		return $user->level;
	}

	public function checkusername($username){
		$check=User::where("name","=",$username)->count();
		if($check!=0)
			return true;
		return false;
    }
	public function checkemail($email){
		$check=User::where("email","=",$email)->count();
		if($check!=0)
			return true;
		return false;
	}

	public function checksocial_id($social_id){
		$check=User::where("Social_id","=",$social_id)->count();
		if($check!=0)
			return true;
		return false;
	}

	public function checkconfirm_code($email, $confirm_code){
		$check=User::where("email","=",$email)->where("confirm_code","=",$confirm_code)->count();
		if($check!=0)
			return true;
		return false;
	}

	public function checkactivation($email){
		$checks = User::where("email","=",$email)->first()->check;
		return $checks;
	}

	public function getNamelogin($email){
		$user = User::where('email','=',$email)->first();
		return $user->name;
	}

	/*public function showlist(){
		$music = User::orderBy('email', 'desc')->get();
		return $music;
	}*/

	public function getinforbyname($name){
		$user = User::where('name','=',$name)->first();
		return $user;
	}

	public function updatename($name, $changename){
		$user = User::where('name','=',$name)->first();
		$user->name=$changename;
		$user->save();
	}

	public function updateemail($name, $changeemail){
		$user = User::where('name','=',$name)->first();
		$user->email=$changeemail;
		$user->save();
	}

	public function updatepass($name, $changepassword){
		$user = User::where('name','=',$name)->first();
		$user->password=md5($changepassword);
		$user->save();
	}

	public function checkemailbyname($name, $changeemail){
		$user = new User;
		$check=$user->checkemail($changeemail);
		if($check==true && $changeemail!=User::getinforbyname($name)->email)
			return true;
		else
			return false;
	}

	public function checknamebyname($name, $changename){
		$user = new User;
		$check=$user->checkusername($changename);
		if($check==true && $changename!=User::getinforbyname($name)->name)
			return true;
		else
			return false;
	}

	public function checkpassbyname($name, $changepass){
		$check=User::where("name","=",$name)->where("password","=",$changepass)->count();
		if($check!=0)
			return true;
		return false;
	}

	public function getinfor(){
		$user = User::where('Social_id','=',"")->orderBy('level')->get();
		return $user;
	}
	
	public function updatecheck($id){
		$user = User::where('id','=',$id)->first();
		if($user->check=='block')
			$user->check='yes';
		else
			$user->check='block';
		$user->save();
		return $user->check;
	}

	//khai
	function checkuser(){
		if(Session::has('user')){$user = User::where('name', '=', session('user'))->first();}
           else {$user = User::where('name', '=', session('admin'))->first();}
             return ($user->id);
         }

}
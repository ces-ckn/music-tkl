<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMusicChartsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('music_charts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('song_id');
			$table->string('image');
			$table->integer('inlist');
			$table->string('title');
			$table->string('artist');
			$table->integer('chart');
			$table->string('linkplay');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('music_charts');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoteInfrosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vote_infros', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('id_user');
			$table->string('song_id');
			$table->integer('vote');//1 down 2/up
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vote_infros');
	}

}

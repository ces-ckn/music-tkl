<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimePlaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('time_plays', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_time');
			$table->integer('chart');
			$table->time('time_start');
			$table->string('time_end',5);
			$table->integer('songs');
			$table->integer('active');
			$table->integer('part');
			$table->integer('choice');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('time_plays');
	}

}

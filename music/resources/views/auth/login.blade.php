@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Login <a href="url">Forgot password?</a></div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST" action="{{ url('login') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-4 control-label">Email</label>
              <div class="col-sm-8">
                <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-4 control-label">Password</label>
              <div class="col-sm-8">
                <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-4 col-sm-8">
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Remember me
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" class="btn btn-default">Sign in</button>
              </div>
            </div>
          </form>
          <div class="sep">
            <span class="or">OR</span>
          </div>
          <div class="col-sm-6">
          <button class="btn btn-lg btn-primary btn-facebook btn-block "> Login with Facebook </btn>
        </div>
        <div class="col-sm-6">
          <button class="btn btn-lg btn-danger btn-block ">Login with Google </btn>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


<div id="mymodal" class="modal fade bs-change-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg"><div class="container">
  <div class="row">
    <div class="col-md-6 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading myfont" style="background-color:#ae4ad9">
          <span><img id="btn_close" class="close" src="image/close.png" width="40px" height="40px" data-dismiss="modal"></span><h4>Change Information</h4>
        </div>

        <div class="panel-body">
          <form id="changeform" class="form-horizontal" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
              <label class="col-md-4 control-label">Username</label>
              <div class="col-md-5">
                <label id="changename"></label>
              </div>
              <div class="col-md-2">
                <a class="btn btn-sm btn-default editname">Edit</a>
              </div>
            </div>

            <hr>

            <div id="newuser" class="form-group alert alert-info">
              <div class="row-lg-9" id="kq1">

              </div>
              <label class="col-md-4 control-label">New Username</label>
              <div class="col-md-5">
                <input type="text" class="form-control" id="name" name='name'>
              </div>
              <div class="col-md-2">
                <a id="up_name" class="btn mybtn">
                  Change
                </a>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">E-Mail</label>
              <div class="col-md-5">
                <label id="changemail"></label>
              </div>
              <div class="col-md-2">
                <a class="btn btn-sm btn-default editmail">Edit</a>
              </div>
            </div>

            <hr>

            <div id="newmail" class="form-group alert alert-info">
              <div class="row-lg-9" id="kq2">

              </div>
              <label class="col-md-4 control-label">New E-Mail</label>
              <div class="col-md-5">
                <input type="email" class="form-control" id='mail' name='email'>
              </div>
              <div class="col-md-2">
                <a id="up_mail" class="btn mybtn">
                  Change
                </a>
              </div>
            </div>

            <div class="form-group">
              <label id="lb_pass" class="col-md-4 control-label">Password</label>
              <div class="col-md-5">
                <label id="lb_pass">Enter button edit to change password</label>
              </div>
              <div class="col-md-2">
                <a class="btn btn-sm btn-default editpass">Edit</a>
              </div>
            </div>

            <hr>

            <div id="passconf" class="form-group alert alert-info">
              <div class="row-lg-9" id="kq3">

              </div>
              <label id="cur_pass" class="col-md-4 control-label">Current Password</label>
              <div class="col-md-5">
                <input type="password" id='pass_cur' class="form-control" name="curpassword">
              </div>
              <label id="lb_pass" class="col-md-4 control-label">New Password</label>
              <div class="col-md-5">
                <input type="password" id='pass' class="form-control">
              </div>
              <label class="col-md-4 control-label">Confirm Password</label>
              <div class="col-md-5">
                <input type="password" id='pass_conf' class="form-control" name="password_confirmation">
              </div>
              <div class="col-md-6 col-md-offset-4">
                <a id="up_pass" class="btn btn-sm mybtn">
                  Change
                </a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script language="javascript">
$(document).ready(function() {
  $('#change').click(function(){
    $.ajax({
    url : "{{asset('getinfor')}}",
    type : "post",
    dateType:"",
    data : {
         _token: token
    },
    success : function (result){
      $('#changename').text(result[0]);
      $('#changemail').text(result[1]);
    }
    });
  });

$("#up_name").click(function(){ 
  var name = $('#name').val();
  if(name != ''){
    $.ajax({
      url : "{{asset('changename')}}",
      type : "post",
      dateType:"",
      data : {
           _token: token, name: name
      },
      success : function (result){
        if(result == name){
          $("div#kq1").load('alert');
          $('#changename').text(result);
          document.getElementById('user_avatar').innerHTML=result;
        }
      }
    });
  }
});

$("#up_mail").click(function(){ 
  var email = $('#mail').val();
  if(email != ''){
    $.ajax({
      url : "{{asset('changeemail')}}",
      type : "post",
      dateType:"",
      data : {
           _token: token, email: email 
      },
      success : function (result){
        if(result == email){
          $("div#kq2").load('alert');
          $('#changemail').text(result);
        }
      }
    });
  } 
});

$("#up_pass").click(function(){ 
  var pass = $('#pass').val();
  var pass_conf = $('#pass_conf').val();
  var pass_cur = $('#pass_cur').val();
  $.ajax({
    url : "{{asset('changepass')}}",
    type : "post",
    dateType:"",
    data : {
         _token: token, password: pass, pass_conf: pass_conf, currentpass: pass_cur  
    },
    success : function (result){
      if(result == 'success')
        $("div#kq3").load('alert');
    }
  });
});

  $(".editname").click(function(){
    $("#newuser").slideToggle("slow");
  });
  $(".editmail").click(function(){
    $("#newmail").slideToggle("slow");
  });
  $(".editpass").click(function(){
    $("#passconf").slideToggle("slow");
  });
});
</script>
<script type="text/javascript ">
$("#changeform").validate({
  rules:{
    name:{
      required:true,
      minlength:4,
      remote:{
        url:"checknamechange",
        type:"get"
      }
    },
    curpassword:{
      required:true,
      minlength:6,
      remote:{
        url:"checkcurrentpass",
        type:"get"
      }
    },
    password:{
      required:true,
      minlength:6
    },
    password_confirmation: {
      required:true,
      equalTo:"#pass"
    },
    email: {
      required:true,
      email: true,
      remote:{
        url:"checkmailchange",
        type:"get"
      }
    }
  },
  messages: {
    curpassword: {
      remote: "Please, review password"
    },
    name:{
      remote:"The username has already been taken."
    },
    email: {
      remote: "The email has already been taken"
    }
  }
});
</script>
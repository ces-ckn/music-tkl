<div class="container">
  <div class="col-md-6 col-md-offset-2 ">
    <div class="panel panel-default">
      <div class="panel-heading myfont" style="background-color:#ae4ad9"><span><img id="cancel" class="close" src="image/close.png" width="40px" height="40px" data-dismiss="modal"></span><h4>Confirm </h4></div>
        <div class="panel-body">
          <form id="confirmform" class="form-horizontal" role="form" method="POST" action="{{ url('confirmhandle') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
              <div class="row-lg-9" id="kq">

              </div>
              <div class="col-md-12">
                <input type="text" class="form-control" name="codeconf" placeholder="for example:thy68i">
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-offset-9">
                <button id="confsubmit" type="submit" class="btn mybtn">
                  OK
                </button>
              </div>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
<script language="javascript">
$("#cancel").click(function(){
  document.location.reload("home");
})

$("#confsubmit").click(function (){
  $.post( $("#confirmform").attr("action"), 
          $("#confirmform :input").serializeArray(),
          function(data){
            if(data=="success")
              document.location.reload("home");
            else
              $("div#kq").html(data);
          });
          $("#confirmform").submit( function(){
            return false;
          });
});
</script>
<div class="container">
  <div class="col-md-6 col-md-offset-2 ">
    <div class="panel panel-default">
      <div class="panel-heading myfont" style="background-color:#ae4ad9"><span><img id="cancel" class="close" src="image/close.png" width="40px" height="40px" data-dismiss="modal"></span><h4>Forgot password</h4></div>
        <div class="panel-body">
          <form id="forgotform" class="form-horizontal" role="form" method="POST" action="{{ url('forgotpasshandle') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
              <div class="row-lg-9" id="kq">

              </div>
              <label class="col-md-4 control-label">E-Mail Address</label>
              <div class="col-md-7">
                <input type="email" class="form-control" name="email" placeholder="Email@example.com">
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-offset-8">
                <button id="forgotsubmit" type="submit" class="btn mybtn">
                  OK
                </button>
              </div>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$("#forgotsubmit").click(function (){
    $.post( $("#forgotform").attr("action"), 
            $("#forgotform :input").serializeArray(),
            function(data){
                $("div#kq").html(data);
            });
            $("#forgotform").submit( function(){
              return false;
            });
  });

$("#cancel").click(function(){
  document.location.reload("home");
})

$("#forgotform").validate({
  rules:{
    email: {
      required:true,
      email: true,
      remote:{
        url:"checkalready",
        type:"get"
      }
    }
  },
  messages: {
    email: {
      remote: "The E-mail isn't already"
    }
  }
})
</script>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">


    <script language="javascript" src="http://code.jquery.com/jquery-2.0.0.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/blog-post.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/mycss.css') }}">
    <script src="{{ asset('assets/js/jquery.js')}}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="{{Asset('assets/js/jquery-validate/jquery.validate.js')}}"></script>
</head>
<body>
  <div class="col-md-5 col-md-offset-3">
    <div class="panel panel-default">
      <div class="panel-heading myfont" style="background-color:#fed136"><h4>Get Password</h4></div>
      <div class="panel-body">
        <form id="getpassform" class="form-horizontal" role="form" method="POST" action="{{ url('getpasswordhandle') }}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">

          <div class="form-group">
            <label class="col-md-4 control-label">New Password</label>
            <div class="col-md-6">
              <input type="password" id="mainpass" class="form-control" name="password" placeholder="Password">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">Confirm Password
            </label>
            <div class="col-md-6">
              <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="getpasssubmit" type="submit" class="btn mybtn">
                Accept
              </button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
<script type="text/javascript ">
$("#getpassform").validate({
  rules:{
    password:{
      required:true,
      minlength:6
    },
    password_confirmation: {
      required:true,
      equalTo:"#mainpass"
    },
  },
});

$("#cancel").click(function(){
  document.location.reload("home");
})
</script>
</body>




<!DOCTYPE html>
<html lang="en">

<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Music Chart - First Project</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/blog-post.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/mycss.css') }}">
    <script src="{{ asset('assets/js/jquery.js')}}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-social.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/flipclock.css') }}">
    <script src="{{Asset('assets/js/jquery-validate/jquery.validate.js')}}"></script>
    <script src="{{Asset('assets/js/flipclock.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('assets/css/style1.css') }}" media="screen">
    <script src="https://cdn.socket.io/socket.io-1.2.0.js"></script>
    <script src="{{ asset('assets/js/bootstrap-select.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.css') }}">


  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 80%;
      margin: auto;
  }
  </style>
            
</head>

<body>
  

<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header myfont">
                <table class="tb">
                  <tr>
                    <th class="tb1"><a class="navbar-brand page-scroll iconm" href="#">
  <img src="http://www.icon100.com/up/1580/512/cd_music_w.png" style="width:42px;height:42px;border:0"></a></th>
                    <th class="tb2">@include("search")</th>
                    <th>
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                    </th>
                  </tr>
                </table>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse myfont" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                   @if(Session::has('user')||Session::has('admin')||Session::has('slave'))
                      @if(Session::has('user')||Session::has('slave'))
                        <li class="dropdown"><a id="user" class="dropdown-toggle" data-toggle="dropdown"><img class="ima" border="0" alt="MusicTKL" src="http://www.clker.com/cliparts/r/4/q/f/d/W/blue-star-edited2-hi.png" width="35" height="32"><span class="votec">{{$vote}}</span><span id="user_avatar"> {{session('user')}}{{session('slave')}} </span><span class="caret"></span></a>
                      @else
                        <li class="dropdown"><a id="user" class="dropdown-toggle" data-toggle="dropdown"><img class="ima" border="0" alt="MusicTKL" src="http://www.clker.com/cliparts/r/4/q/f/d/W/blue-star-edited2-hi.png" width="35" height="32"><span class="votec">{{$vote}}</span><span id="user_avatar"> {{session('admin')}} </span><span class="caret"></span></a>
                      @endif  
                        <ul class="dropdown-menu"> 
                        @if(Session::has('user')||Session::has('slave'))
                        @if(Session::has('slave'))
                        <li><a class="Khai_user" id="view" data-toggle="modal" data-target=".bs-change-modal-lg">view<a></li>
                        @else
                         <li><a class="Khai_user" id="change" data-toggle="modal" data-target=".bs-change-modal-lg">Change<a></li>
                        @endif 
                        @else
                          <li><a class="Khai_user" id="user_mana" href="{{ url('inforaccount') }}">user management<a>
                        @endif                     
                        <li><a id="btn_logout" href="{{ url('logout') }}">Logout</a></li>
                        </ul>
                        </li>
                      @else
                        <table class="tbmenu">
                          <tr>
                            <th><br><a id="btn_login" data-toggle="modal" data-target=".bs-login-modal-lg">Login</a></th>              
                            <th><br><a id="btn_login">&nbsp;&nbsp;/&nbsp;&nbsp;</a></th>
                            <th><br><a id="btn_register" data-toggle="modal" data-target=".bs-register-modal-lg">Register</a></th>
                          </tr>
                        </table>
                      @endif
                </ul>
            </div>


           <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="body1">
    <div class="container">
      <div class ="col-sm-12">
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-9">
                  
               </div><!-- /.col-lg-6 -->
              <div class="col-lg-3 col-lg-offset-0">
                <div class="thame" id="linh_thame" style="margin:2em;">
              
                </div> 
                <div class="message"></div>
            </div>
        </div>
      </div>



        <div class="row body2">
          <div class="col-lg-12">
            <!-- Blog Post Content Column -->
            <div class="col-lg-9">
                <!-- Blog Post -->
          
          <div class="row">
            @include("play")
          </div>  
                <!-- slide -->
                <!-- Preview Image -->
      <input type="hidden" id="all_songs" value="{{$music->count()}}">
      <div class="list" id="result"> 
            @include("list")
           </div>

              
     </div><!--lg-8  -->

          <div class="col-lg-3">
             <div class="vote">
                 @include("settime")
              </div>
           </div><!--thong tin vote -->
     </div>
  </div>

      <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->
</div>
@include('login')
@include('register')
@include('changeinformal')
  </div>
</div>
<div class="message"></div>
</body>
</html>

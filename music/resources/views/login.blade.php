<!-- login -->
<!-- jQuery ------------------------n login -->
<div id="mymodal" class="modal fade bs-login-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
 <!--   <div class="modal-content">  -->
    <div class="container">
    <div class="col-md-5 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading myfont" style="background-color:#ae4ad9"><span><img id="cancel" class="close" src="image/close.png" width="40px" height="40px" data-dismiss="modal"></span><h4>Login <a id="forgot" ><h6 style="color:#ffffff">Forgot password?</h6></a></h4></div>
        <div class="panel-body">
          <form class="form-horizontal" id="loginform" role="form" method="POST" action="{{ url('loginhandle') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row-lg-9" id="kq">

            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-4 control-label">Email</label>
              <div class="col-sm-8">
                <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-4 control-label">Password</label>
              <div class="col-sm-8">
                <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-4 col-sm-8">
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Remember me
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-4 col-sm-8">
                <button id="submit" type="submit" class="btn mybtn">Sign in</button>
              </div>
            </div>
          </form>
          <div class="sep">
            <span class="or">OR</span>
          </div>
          <div class="row-sm-6">
          <a href="{{ url('loginfacehandle?login') }}" class="btn btn-sm btn-social btn-facebook btn-block"><i class="fa fa-facebook"></i>Login with Facebook </a>
        </div>
        <div class="form-group">

        </div>
        <div class="row-sm-6">
          <a href="{{ url('logingooglehandle?login') }}" class="btn btn-sm btn-social btn-google btn-block"><i class="fa fa-google"></i>Login with Google </a>
        </div>
        </div>
      </div>
    </div>
  </div>
<script language="javascript">
  $("#forgot").click(function(){
    $("div#mymodal").load("forgotpass");
  });
  
  $("#submit").click(function (){
    $.post( $("#loginform").attr("action"), 
            $("#loginform :input").serializeArray(),
            function(data){
              if(data=="login"){
                document.location.reload("home");
              }
              if(data=="confirm"){
                $("div#mymodal").load("confirm");
              }
              else{
                $("div#kq").html(data);
              }
            });
            $("#loginform").submit( function(){
              return false;
            });
  });
</script>
<script type="text/javascript">
$("#loginform").validate({
  rules:{
    password:{
      required:true,
      minlength:6
    },
    email: {
      required:true,
      email: true,
    }
  },
})
</script>
</div>
</div>
<!-- end div login -->
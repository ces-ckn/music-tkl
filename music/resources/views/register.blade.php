<!-- register -->
<div id="myregismodal" class="modal fade bs-register-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
  <div class="container-fluid">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading myfont" style="background-color:#ae4ad9"><span><img id="cancel" class="close" src="image/close.png" width="40px" height="40px" data-dismiss="modal"></span><h4>Register</h4></div>
        <div class="panel-body">
          <form id="registerform" class="form-horizontal" role="form" method="POST" action="{{ url('registerhandle') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
              <label class="col-md-4 control-label">Username</label>
              <div class="col-md-6">
                <input type="text" class="form-control" name="name" placeholder="Username">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">E-Mail Address</label>
              <div class="col-md-6">
                <input type="email" class="form-control" name="email" placeholder="Email@example.com">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Password</label>
              <div class="col-md-6">
                <input type="password" id="mainpass" class="form-control" name="password" placeholder="Password">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Confirm Password</label>
              <div class="col-md-6">
                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button id="regissubmit" type="submit" class="btn mybtn">
                  Register
                </button>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript">
  $("#regissubmit").click(function (){
    $.post( $("#registerform").attr("action"), 
            $("#registerform :input").serializeArray(),
            function(data){
              if(data == 'success')
                $("div#myregismodal").load('confirm');
            });
            $("#registerform").submit( function(){
              return false;
            });
  });
</script>
<script type="text/javascript ">
$("#registerform").validate({
  rules:{
    name:{
      required:true,
      minlength:4,
      remote:{
        url:"checkusername",
        type:"get"
      }
    },
    password:{
      required:true,
      minlength:6
    },
    password_confirmation: {
      required:true,
      equalTo:"#mainpass"
    },
    email: {
      required:true,
      email: true,
      remote:{
        url:"checkemail",
        type:"get"
      }
    }
  },
  messages: {
    name:{
      remote:"The username has already been taken."
    },
    email: {
      remote: "The email has already been taken"
    }
  }
})
</script>
  </div>
</div>
<!-- end div register -->
    <input type="hidden" id="choose_action">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="number_add" value="0">
    <input type="hidden" id="separation">
    <input type="hidden" id="after">
    
    <div class="list-group" >
     
        <li href="#" class="list-group-item disabled" style="background-color: #FCFCFC;">
          <span id="linh_title">TIME ZONE</span>
           @if(Session::has('admin'))
           <button type="button" id="linh_add_btn" class="btn btn-primary linh_add_btn">ADD</button>
           @endif
           <div id="test"></div>
        </li>
      <ol id="linh_ol">
      </ol>
    </div>


    <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" id="linh_settime">
        <div class="modal-content" id="linh_model_content">
          <div class="panel panel-default " id="linh_default">
            <!-- Default panel contents -->
            <div class="panel-heading linh_panel" id="panel_start" style="background:black; color:white">Start</div>
            <!-- Table -->
            <div id="start_table">
              <table class="table " id="linh_table">
                <input type="hidden" id="choose_text">
               
                <tr>
                  <td>Hours</td>
                  <td>Minutes</td>
                </tr>
                <tr>
                  <td><span class="linh_hover" onclick="sethour('plus','hour','minute')"><img src="{{Asset('/assets/img/arrowup.png')}}"></span></td>
                  <td><span class="linh_hover" onclick="setmin('plus','minute','hour')"><img src="{{Asset('/assets/img/arrowup.png')}}"></span></td>
                </tr>
                <tr>
                  <td class="linh_hover"><input type="text" name="hour" id="hour" onClick="this.setSelectionRange(0, this.value.length)" onkeypress="return getvalue('hour',event,'hour')" onkeydown="return getvalue_keydown('hour',event,'hour','minute')" value="00" placeholder="00" autofocus></td>
                  <td class="linh_hover"><input type="text" name="minute" id="minute" onClick="this.setSelectionRange(0, this.value.length)" onkeypress="return getvalue('minute',event,'minute')" onkeydown="return getvalue_keydown('minute',event,'minute','hour')" value="00" placeholder="00"></td>
                </tr>
                <tr>
                  <td><span class="linh_hover" onclick="sethour('minus','hour','minute')"><img src="{{Asset('/assets/img/arrowdown.png')}}"></span></td>
                  <td><span class="linh_hover" onclick="setmin('minus','minute','hour')"><img src="{{Asset('/assets/img/arrowdown.png')}}"></span></td>
                </tr>
                <tr id="songs_area">
                  <td >
                    <select onchange="choice()" id="choice" class="selectpicker show-tick form-control">
                      <option value="end">End time</option>
                      <option value="songs">Songs</option>
                      <option value="limit">No limit</option>
                    </select>
                  </td> 
                  
                  <td >
                    <input type="text" id="number_songs" style="display:none" onClick="this.setSelectionRange(0, this.value.length)" onkeypress="return getvalue('song',event)" onkeydown="return getvalue_keydown('song',event)" value="01" placeholder="00">
                    <div id="set_end">
                      <input type="text" name="hour_end" id="hour_end" class="end_time" onClick="this.setSelectionRange(0, this.value.length)" onkeypress="return getvalue('hour',event,'hour_end')" onkeydown="return getvalue_keydown('hour',event,'hour_end','min_end')" value="01" placeholder="00">
                      <span>:</span> 
                      <input type="text" name="min_end" id="min_end" class="end_time" onClick="this.setSelectionRange(0, this.value.length)" onkeypress="return getvalue('minute',event,'min_end')" onkeydown="return getvalue_keydown('minute',event,'min_end','hour_end')" value="01" placeholder="00">
                    </div> 
                   </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <span class="linh_done" onclick="complete()" class="btn btn-default" data-dismiss="modal">Done</span>
                  </td>
                </tr> 
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

<script type="text/javascript">
 var socket= io.connect('localhost:3000');
  //var socket= io.connect('http://192.168.1.111:3000');
  var token=$('input[name=_token]').val();
  var img_del="{{Asset('/assets/img/del.png')}}";
  function choice(){
    var temp = $("#choice").val();
     if(temp== "songs"){
      document.getElementById("number_songs").style.display="table-cell";   
      document.getElementById("set_end").style.display="none";
      document.getElementById("number_songs").disabled=false;   
      document.getElementById("hour_end").disabled=true; 
      document.getElementById("min_end").disabled=true;  
    }else if(temp== 'end'){
      document.getElementById("number_songs").style.display="none";   
      document.getElementById("set_end").style.display="table-cell";
      document.getElementById("number_songs").disabled=true;   
      document.getElementById("hour_end").disabled=false; 
      document.getElementById("min_end").disabled=false;  
    }else{
      document.getElementById("number_songs").style.display="none"; 
      document.getElementById("set_end").style.display="none";
      document.getElementById("number_songs").disabled=true;   
      document.getElementById("hour_end").disabled=true; 
      document.getElementById("min_end").disabled=true; 
    }
  }
  function display_choice(id,choice){
    if(choice == 0){
      document.getElementById("for_song"+id).style.display="none";
      document.getElementById("text_end"+id).style.display="inline";
      document.getElementById("limit"+id).style.display="none";
    }else if(choice == 1){
      document.getElementById("for_song"+id).style.display="inline";
      document.getElementById("text_end"+id).style.display="none";
      document.getElementById("limit"+id).style.display="none";
    }else{
      document.getElementById("for_song"+id).style.display="none";
      document.getElementById("text_end"+id).style.display="none";
      document.getElementById("limit"+id).style.display="inline";
    }
  }
  function get_choice(){
    var temp = $('#choice').val();
    if(temp == "end"){
      return 0;
    }else if(temp == "songs"){
      return 1;
    }else{
      return 2;
    }
  }
 
  function changestatus(id){
    var idtime=document.getElementById('idtime'+id).value;     
    var active=document.getElementById('myonoffswitch'+id).checked; 
    var start= document.getElementById('text_start'+id).value;  
    var chart = document.getElementById('chart'+id).value;
    
    var number_add = document.getElementById('number_add').value;
    var songs= document.getElementById('songs'+id).value;
    var k = document.getElementById('separation').value;
    if(active){
      //off
      var act = 0;
      if(IsHead(id)){
        settime(id,"off");
      }        
      arrangechart(number_add,id,act,k);  
        
    }else{  
      //on
      var act  = 1;
      arrangechart(number_add,id,act,k);
      if(IsHead(id)){
        settime(id,"on");
      }
    }   
    if(k>0){
      $.ajax({
        url: "{{Asset('/arrangechart')}}",
        type: 'post',
        data: {idtime:idtime,act:act,diff:k,chart:chart,_token: token},
        success: function(data){}    
      },"json");
    }

    /*if(id != 0){}else{
      if(active){
        var act = 0;
      }else{
        var act = 1;
      }
      var songs= document.getElementById('all_songs').value;
      save(id,idtime,chart,start,songs,act);
    }*/
      socket.emit('changestatus',id,number_add,start,songs,idtime,active,k,chart);
  }
  socket.on('changestatus',function(id,number_add,start,songs,idtime,active,k,chart){
      if(id != 0 ){
        if(active){
          //off
          var act = 0;
          if(IsHead(id)){
            settime(id,"off");
          }        
          arrangechart(number_add,id,act,k);  
            
        }else{  
          //on
          var act  = 1;
          arrangechart(number_add,id,act,k);
          if(IsHead(id)){
            settime(id,"on");
          }
        }
      }else{
        if(active){
          //off
          document.getElementById('myonoffswitch0').checked=true; 
        }else{  
          //on
          document.getElementById('myonoffswitch0').checked=false; 
        }
      }     
  });
  function settime(id,stt){
    document.getElementById('choose_action').value=0;
    clock.setTime(0);
    clock.stop();
    if(stt=="off" || stt=="delete" || stt=="theend"){  
      for (var i = 1; i <= 5; i++) {
        if(document.getElementById('myonoffswitch'+i)){
          var active=document.getElementById('myonoffswitch'+i).checked;  
          var chart = document.getElementById('chart'+i).value;
          if(!active && chart==2){
            var start = document.getElementById('text_start'+i).value;
            var start = estimate_time(start);
            document.getElementById('choose_action').value=1;
            clock.setTime(start);
            clock.start();
            break;
          }
        }
      };
    }else if(stt=="on"){
      var active=document.getElementById('myonoffswitch'+id).checked;  
      var chart = document.getElementById('chart'+id).value;
      var start = document.getElementById('text_start'+id).value;
      var start = estimate_time(start);
      document.getElementById('choose_action').value=1;
      clock.setTime(start);
      clock.start();
    }    
  }
  function estimate_time(start){
    var now=get_time_now();
    var day = 23*60*60 + 59*60 + 59;
      var start = start.substr(0,5);
      var hour = parseInt(start.substr(0, 2));
      var min = parseInt(start.substr(-2, 2));
      var minus= hour * 60 * 60 + min * 60 - now;
      if(minus< 0){
        minus=day+minus;
      }else{
        minus=minus;
      }      
    return minus;                   
  }
  function IsHead(id){
    var chart = document.getElementById('chart'+id).value;
    if(chart == 1){
      return true;
    }else{
      return false;
    }
  }
  function setafter(){
    var j= 0;
    var chart_min=0;
    number_add = 5;
    for (var i = 1; i <= number_add; i++) {
        if(document.getElementById('myonoffswitch'+i)){
          var act = document.getElementById('myonoffswitch'+i).checked;
          if(!act){
            var chart= document.getElementById('chart'+i).value;
            if(chart > chart_min){
              chart_min=chart;
              j=i;
            }   
          }  
        }    
    }
    document.getElementById('after').value=j;
  }
  function arrangechart(number_add,id,active,separation){
    var arr=[];
    //setafter();
    //var temp = document.getElementById('after').value;
    var j= 0;
    var chart_min=0;
    number_add = 5;
    for (var i = 1; i <= number_add; i++) {
      if(i!=id){ 
        if(document.getElementById('myonoffswitch'+i)){
          var act = document.getElementById('myonoffswitch'+i).checked;
          if(!act){
            var chart= document.getElementById('chart'+i).value;
            if(chart > chart_min){
              chart_min=chart;
              j=i;
            }   
          }  
        }    
      }
    }

    document.getElementById('after').value=j;
    var start= document.getElementById('text_start'+id).value;
    var songs= document.getElementById('songs'+id).value;
    var choice = $('#choice'+id).val();
    var end = $('#text_end'+id).val();
    var idtime= document.getElementById('idtime'+id).value;
    for (var i = 1; i <= number_add; i++) {
      if(document.getElementById('chart'+i)){
        arr[i]=document.getElementById('chart'+i).value;
      }else{
        arr[i]=0;
      }    
    };
    var max = arr[1];
    for (var i = 1; i <= number_add; i++) {
      if(arr[i]>max){
        max=arr[i];
      }    
    };
           
    if(active==0){
      for (var i = 1; i <= number_add; i++) {
        if(arr[i]!=0){
          if(arr[i] > arr[id]){
            arr[i] = parseInt(arr[i])-1;
          }
          document.getElementById('chart'+i).value=arr[i];
        }
      };
      document.getElementById('chart'+id).value=max; 
      document.getElementById('separation').value=parseInt(separation)-1;
      setTimeout(function(){
        $("#part"+id).remove();
        $("#linh_ol").append('<div id="part'+id+'"><li class="list-group-item my_item"><input type="hidden" id="choice'+id+'" value="'+choice+'"><input type="hidden" id="number_text'+id+'" value="'+id+'"><input type="hidden" id="idtime'+id+'" value="'+idtime+'"><div id="show_time"><input type="hidden" id="chart'+id+'" value="'+max+'"><input type="text" class="text_class" id="text_start'+id+'" placeholder="00:00" value="'+start+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg @endif" onclick="setcurrent('+id+')"> &raquo; <div id="change_area"><input type="text" class="text_class text_end" id="text_end'+id+'" placeholder="00:00" value="'+end+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" @endif onclick="setcurrent('+id+')"><div id="for_song'+id+'"><input type="text" class="text_class_songs" id="songs'+id+'" placeholder="00:00" value="'+songs+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" @endif onclick="setcurrent('+id+')"><label for="songs'+id+'"><span class="character">Songs</span></label></div><span id="limit'+id+'" class="limit_character">No Limit</span></div></div>@if(Session::has("admin"))<div class="del" id="delete'+id+'" onclick="delete_time('+id+')"><img src="'+img_del+'"></div>@endif<div class="onoffswitch my_onoffswitch"><input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch'+id+'" checked="" @if(Session::has("admin")) onclick=changestatus('+id+') @else disabled @endif><label class="onoffswitch-label" for="myonoffswitch'+id+'" ><span class="onoffswitch-inner" ></span><span class="onoffswitch-switch"></span></label></div></li></div>');                          
        display_choice(id,choice);
      }, 300);    
    }else{
      for (var i = 1; i <= number_add; i++) {
        if(arr[i]!=0){
          if(arr[i] >= separation){
            arr[i] = parseInt(arr[i])+1;         
          }
          document.getElementById('chart'+i).value=arr[i];
        }  
      };
      for (var i = 1; i <= number_add; i++) {
        if(arr[i]!=0){
          if(arr[i] > arr[id]){
            arr[i] = parseInt(arr[i])-1;
            document.getElementById('chart'+i).value=arr[i];
          }
        }
      };
      document.getElementById('chart'+id).value=separation; 
      document.getElementById('separation').value=parseInt(separation)+1;
      var temp = document.getElementById('after').value;
      setTimeout(function(){
        $("#part"+id).remove();
        $("#part"+temp).after('<div id="part'+id+'"><li class="list-group-item my_item"><input type="hidden" id="choice'+id+'" value="'+choice+'"><input type="hidden" id="number_text'+id+'" value="'+id+'"><input type="hidden" id="idtime'+id+'" value="'+idtime+'"><div id="show_time"><input type="hidden" id="chart'+id+'" value="'+separation+'"><input type="text" class="text_class" id="text_start'+id+'" placeholder="00:00" value="'+start+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg @endif" onclick="setcurrent('+id+')"> &raquo; <div id="change_area"><input type="text" class="text_class text_end" id="text_end'+id+'" placeholder="00:00" value="'+end+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" @endif onclick="setcurrent('+id+')"><div id="for_song'+id+'"><input type="text" class="text_class_songs" id="songs'+id+'" placeholder="00:00" value="'+songs+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" @endif onclick="setcurrent('+id+')"><label for="songs'+id+'"><span class="character">Songs</span></label></div><span id="limit'+id+'" class="limit_character">No Limit</span></div></div>@if(Session::has("admin"))<div class="del" id="delete'+id+'" onclick="delete_time('+id+')"><img src="'+img_del+'"></div>@endif<div class="onoffswitch my_onoffswitch"><input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch'+id+'" @if(Session::has("admin"))onclick=changestatus('+id+') @else disabled @endif><label class="onoffswitch-label" for="myonoffswitch'+id+'" ><span class="onoffswitch-inner" ></span><span class="onoffswitch-switch"></span></label></div></li></div>');       
        display_choice(id,choice);
      }, 300);  
    }   
  }

  function complete(){
    var cur =document.getElementById('choose_text').value;
    var hour = document.getElementById('hour').value;
    var minute = document.getElementById('minute').value;
    var start = hour+":"+minute;
    var hour_end= $('#hour_end').val();
    var min_end=$('#min_end').val();
    var end = hour_end+":"+min_end;
    var choice = get_choice();
    if(cur != 0){
      var songs = document.getElementById('number_songs').value;
    }else{
      var songs = document.getElementById('all_songs').value;
    }
    socket.emit('done',cur,start,songs,end,choice);
  };
  socket.on('done',function(cur,start,songs,end,choice){
    done(cur,start,songs,end,choice);
  });
  function done(cur,start,songs,end,choice){
    document.getElementById('text_start'+cur).value=start;
    var idtime=document.getElementById('idtime'+cur).value;
    var chart=document.getElementById('chart'+cur).value;  
    var active = document.getElementById('myonoffswitch'+cur).checked; 
    $('#choice'+cur).val(choice);
    display_choice(cur,choice);
    document.getElementById('songs'+cur).value=songs;
    var number_add = document.getElementById('number_add').value;
    if(active){
      var act =0;
    }else{
      var act=1;
      if(IsHead(cur)){
        settime(cur,'on');
      }
    }
    $('#text_end'+cur).val(end);
    save(cur,idtime,chart,start,songs,act,end,choice);
  }
  
  function save(part,idtime,chart,start,songs,act,end,choice){
    $(document).ready(function(){    
      $.ajax({
        url: "{{Asset('/savetime')}}",
        type: 'post',
        data: {part:part,idtime:idtime,chart:chart,start:start, songs:songs,act:act,end:end,choice:choice,_token: token},
        success: function(data){}    
      },"json");
    });
  }
  function setcurrent(id){
    var choice = $('#choice'+id).val();
    if(choice==0){
      $('.selectpicker').selectpicker('val', 'end');
      document.getElementById("number_songs").style.display="none";   
      document.getElementById("set_end").style.display="table-cell";
      document.getElementById("number_songs").disabled=true;   
      document.getElementById("hour_end").disabled=false; 
      document.getElementById("min_end").disabled=false;  
    }else if(choice==1){
      $('.selectpicker').selectpicker('val', 'songs');
      document.getElementById("number_songs").style.display="table-cell";   
      document.getElementById("set_end").style.display="none";
      document.getElementById("number_songs").disabled=false;   
      document.getElementById("hour_end").disabled=true; 
      document.getElementById("min_end").disabled=true;    
    }else{
      $('.selectpicker').selectpicker('val', 'limit');
      document.getElementById("set_end").style.display="none";
      document.getElementById("number_songs").style.display="none";  
      document.getElementById("number_songs").disabled=true;   
      document.getElementById("hour_end").disabled=true; 
      document.getElementById("min_end").disabled=true; 
    }
   
    var songs = document.getElementById('songs'+id).value;
    document.getElementById('number_songs').readOnly=false;
    document.getElementById('number_songs').value=songs;  
    document.getElementById('choose_text').value=id;
    var get_text_start = document.getElementById('text_start'+id).value;
    var first_start= get_text_start.substr(0, 2);
    var last_start = get_text_start.substr(-2, 2);
    document.getElementById('hour').value=first_start;
    document.getElementById('minute').value=last_start;
    var text_end = $('#text_end'+id).val();
    var first_end= text_end.substr(0, 2);
    var last_end = text_end.substr(-2, 2);
    document.getElementById('hour_end').value=first_end;
    document.getElementById('min_end').value=last_end;
  }
  socket.on('add_time',function(number_add,id,idtime){   
      if(number_add > 5){
        document.getElementById('number_add').value=5;
      }else{
        document.getElementById('number_add').value=number_add;
      }   
      $("#linh_ol").append('<div id="part'+id+'"><li class="list-group-item my_item" ><input type="hidden" id="choice'+id+'" value="0"><input type="hidden" id="number_text'+id+'" value="'+id+'"><input type="hidden" id="idtime'+id+'" value="'+idtime+'"><input type="hidden" id="chart'+id+'" value="'+number_add+'"><div id="show_time"><input type="text" class="text_class" id="text_start'+id+'" placeholder="00:00" value="00:00" readonly class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg " onclick="setcurrent('+id+')"> &raquo; <div id="change_area"><input type="text" class="text_class text_end" id="text_end'+id+'" placeholder="00:00" value="00:00" readonly class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg " onclick="setcurrent('+id+')"></div></div>@if(Session::has("admin"))<div class="del" id="delete'+id+'" onclick="delete_time('+id+')"><img src="'+img_del+'"></div>@endif<div class="onoffswitch my_onoffswitch"><input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch'+id+'" checked="" @if(Session::has("admin"))onclick=changestatus('+id+')@else disabled @endif><label class="onoffswitch-label" for="myonoffswitch'+id+'" ><span class="onoffswitch-inner" ></span><span class="onoffswitch-switch"></span></label></div></li></div>');
  });
  $("#linh_add_btn").click(function(){
      var end = "00:00";
      var songs = "01";
      var number_add= parseInt(document.getElementById('number_add').value) + 1;
      var ext = number_add;
      var ext = IsnotExist(number_add);
      var idtime = Math.floor((Math.random() * 100000) + 1);    
      if(number_add > 1){
          for (var i = 1; i <= number_add; i++) {
            if(i!=ext){
              if(document.getElementById('idtime'+i)){
                var id_time=document.getElementById('idtime'+i).value;    
                if(id_time==idtime){
                  var idtime = Math.floor((Math.random() * 100000) + 1);
                  i=i-1;
                }
              } 
            }
        };
      }  
      id=ext;
      if(number_add==5){
        document.getElementById('linh_add_btn').style.display="none";
      }
      if(number_add > 5){
        document.getElementById('number_add').value=5;
      }else{
        document.getElementById('number_add').value=number_add;
      }
      
      $("#linh_ol").append('<div id="part'+id+'"><li class="list-group-item my_item"><input type="hidden" id="choice'+id+'" value="0"><input type="hidden" id="number_text'+id+'" value="'+id+'"><input type="hidden" id="idtime'+id+'" value="'+idtime+'"><input type="hidden" id="chart'+id+'" value="'+number_add+'"><div id="show_time"><input type="text" class="text_class" id="text_start'+id+'" placeholder="00:00" value="00:00" readonly class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg " onclick="setcurrent('+id+')"> &raquo; <div id="change_area"><input type="text" class="text_class text_end" id="text_end'+id+'" placeholder="00:00" value="'+end+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" @endif onclick="setcurrent('+id+')"><div id="for_song'+id+'"><input type="text" class="text_class_songs" id="songs'+id+'" placeholder="00:00" value="'+songs+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" @endif onclick="setcurrent('+id+')"><label for="songs'+id+'"><span class="character">Songs</span></label></div><span id="limit'+id+'" class="limit_character">No Limit</span></div></div>@if(Session::has("admin"))<div class="del" id="delete'+id+'" onclick="delete_time('+id+')"><img src="'+img_del+'"></div>@endif<div class="onoffswitch my_onoffswitch"><input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch'+id+'" checked="" @if(Session::has("admin"))onclick=changestatus('+id+')@else disabled @endif><label class="onoffswitch-label" for="myonoffswitch'+id+'" ><span class="onoffswitch-inner" ></span><span class="onoffswitch-switch"></span></label></div></li></div>');
      document.getElementById("for_song"+id).style.display="none";
      document.getElementById("text_end"+id).style.display="inline";
      document.getElementById("limit"+id).style.display="none";
      var start = document.getElementById('text_start'+id).value;
      var choice = "0";
      var end_time= end;
      var act = 0;
      var chart = number_add;
      save(id,idtime,chart,start,songs,act,end,choice);
      socket.emit('add_time',number_add,id,idtime);
  });

  function IsnotExist(number_add){
    for (var i = 1; i <= 5; i++) {
      var idtime = document.getElementById('idtime'+i);
      if(!idtime){
        return i;
      }
    };
  }
  function delete_time(id){
    var id_time = document.getElementById('idtime'+id).value;
    var number_add=document.getElementById('number_add').value;
    var chart=document.getElementById('chart'+id).value;
    var active = document.getElementById('myonoffswitch'+id).checked;
    if(number_add<=5){
        document.getElementById('linh_add_btn').style.display="inline";
    }
    document.getElementById('number_add').value = number_add -1;
    var temp = document.getElementById('number_add').value;
    if(temp == 0){
      document.getElementById('choose_action').value=0;
      clock.stop();
      clock.setTime(0);
    }
    if(active){
      var act = 0;
    }else{
      var act = 1;
      var separation = document.getElementById('separation').value;
      document.getElementById('separation').value=parseInt(separation)-1;
    }
    if(IsHead(id)){
      settime(id,'delete');
    }
    arrange_after_del(id,number_add,chart);
    $("#part"+id).remove();
    $.ajax({
      url: "{{Asset('/deletetime')}}",
      type: 'post',
      data: {idtime:id_time,chart:chart,_token: token},
      success: function(data){
      }      
    },"json");
    socket.emit('deletetime',id,id_time,number_add,chart,active); 
  }
  socket.on('deletetime',function(id,id_time,number_add,chart,active){
    document.getElementById('number_add').value = number_add -1;
    var temp = document.getElementById('number_add').value;
    if(temp == 0){
      document.getElementById('choose_action').value=0;
      clock.stop();
      clock.setTime(0);
    }
    if(active){
      var act = 0;
    }else{
      var act = 1;
      var separation = document.getElementById('separation').value;
      document.getElementById('separation').value=parseInt(separation)-1;
    }
    if(IsHead(id)){
      settime(id,'delete');
    }
    arrange_after_del(id,number_add,chart);
    $("#part"+id).remove();
  });
  function arrange_after_del(id,number_add,chart){

    for (var i = 1; i <=5; i++) {
      if(i != id && document.getElementById('chart'+i)){
          var chart_temp=document.getElementById('chart'+i).value;
          if(chart < chart_temp){
            chart_temp=chart_temp-1;
            document.getElementById('chart'+i).value=chart_temp;

          }
      }  
    };
  }
 
  function sethour(stt,stt2,stt3){
    if(stt2=="hour"){
      var h = parseInt($('input[name=hour]').val());
    }else{
      var h = parseInt($('input[name=hour_end]').val());
    }
    if(stt == "plus"){
      if(h == "00"){
        document.getElementById(''+stt2).value="01";
      }else{
        var h = h+1;
        if(h<24){
          if(checkonly(h)){
            document.getElementById(''+stt2).value="0"+h;
          }else{
            document.getElementById(''+stt2).value=h;
          }
        }else{
           document.getElementById(''+stt2).value="00"; 
        }
      }
      
    }else if(stt =="minus"){
      if(stt2=="hour"){
        var h = parseInt($('input[name=hour]').val());
      }else{
        var h = parseInt($('input[name=hour_end]').val());
      }
      if(h == "00"){
        document.getElementById(''+stt2).value="23";
      }else{
        var h = h-1;
        if(h>0){
          if(checkonly(h)){
            document.getElementById(''+stt2).value="0"+h;
          }else{
            document.getElementById(''+stt2).value=h;
          }
        }else{
           document.getElementById(''+stt2).value="00"; 
        }
      }
    }
  }
  function setmin(stt,stt2,stt3){
    if(stt2=="minute"){
      var m = parseInt($('input[name=minute]').val());
    }else{
      var m = parseInt($('input[name=min_end]').val());
    }
    if(stt == "plus"){
      if(m == "00"){
        document.getElementById(''+stt2).value="01";
      }else{
        var m = m +1;

        if(m<60){
          if(checkonly(m)){
            document.getElementById(''+stt2).value="0"+m;
          }else{
            document.getElementById(''+stt2).value=m;
          }
        }else{
          document.getElementById(''+stt2).value="00"; 
          sethour('plus',stt3,stt2);
        }
      }
      
    }else if(stt =="minus"){
      if(m == "00"){
        document.getElementById(''+stt2).value="59";
        sethour('minus',stt3,stt2);
      }else{
        if(stt2=="minute"){
          var m = parseInt($('input[name=minute]').val());
        }else{
          var m = parseInt($('input[name=min_end]').val());
        }
        var m = m -1;
        if(m>=0){
          if(checkonly(m)){
            document.getElementById(''+stt2).value="0"+m;
          }else{
            document.getElementById(''+stt2).value=m;
          }
        }else{
           document.getElementById(''+stt2).value="59"; 
           sethour('minus',stt3,stt2);
        }
      }
    }
  }
  function checkonly(number){
    var number = number.toString();
    if(number.length < 2){
      return true;
    }else{
      return false;
    }
  }
  //For Chrome
  function getvalue_keydown(stt,event,stt2,stt3){
    var charCode = (event.which) ? event.which : event.keyCode; 
    if(stt=="hour"){
      if(charCode == 8){
        var h = document.getElementById(''+stt2).value;
        var temp = h.substr(0, 1);
        document.getElementById(''+stt2).value ="0"+temp;
        return false;
      }
      if(charCode == 38){
        sethour('plus',stt2,stt3);
        return false;
      }
      if(charCode == 40){
        sethour('minus',stt2,stt3);
        return false;
      }
      if(charCode == 9){
        var range = document.getElementById(''+stt3).value.length;
        document.getElementById(''+stt3).focus();
        document.getElementById(''+stt3).setSelectionRange(0,range);
        return false;
      }
    }else if(stt=="minute"){
      if(charCode == 8){
        var min = document.getElementById(''+stt2).value;
        var temp = min.substr(0, 1);
        document.getElementById(''+stt2).value ="0"+temp;
        return false;
      }
      if(charCode == 38){
        setmin('plus',stt2,stt3);
        return false;
      }
      if(charCode == 40){
        setmin('minus',stt2,stt3);
        return false;
      }
      if(charCode == 9){
        var choice = get_choice();
        if(stt2=="minute"){
          if(choice == 0){
            var range = document.getElementById('hour_end').value.length;
            document.getElementById('hour_end').focus();
            document.getElementById('hour_end').setSelectionRange(0,range);
          }else if(choice == 1){
            var range = document.getElementById('number_songs').value.length;
            document.getElementById('number_songs').focus();
            document.getElementById('number_songs').setSelectionRange(0,range);
          }else{
            var range = document.getElementById('hour').value.length;
            document.getElementById('hour').focus();
            document.getElementById('hour').setSelectionRange(0,range);
          }
        }else if(stt2 =="min_end"){
          var range = document.getElementById('hour').value.length;
          document.getElementById('hour').focus();
          document.getElementById('hour').setSelectionRange(0,range);
        } 
        return false;
      }
    }else if(stt == "song"){
      var choose_text =document.getElementById('choose_text').value;
      var songs = document.getElementById('all_songs').value;
      var songs = parseInt(songs);
      if(choose_text==0){

      }else{
        if(charCode == 38){
          setnumbersong('plus',songs);
          return false;
        }
        if(charCode == 40){
          setnumbersong('minus',songs);
          return false;
        }
      }
      if(charCode == 9){
        var range = document.getElementById('hour').value.length;
        document.getElementById('hour').focus();
        document.getElementById('hour').setSelectionRange(0,range);
        return false;
      }
    }
  }
  function getvalue(stt,event,stt2){
    var charCode = (event.which) ? event.which : event.keyCode;
    
    if(stt=="hour"){
      if(stt2=="hour"){
        var h = $('input[name=hour]').val();
      }else{
        var h = $('input[name=hour_end]').val();
      }
      if(event.ctrlKey || event.altKey){
        return false;
      }
      /*if(charCode == 9){
        var range = document.getElementById('minute').value.length;
        document.getElementById('minute').focus();
        document.getElementById('minute').setSelectionRange(0,range);
        return false;
      }

       Firefox
      if(charCode == 38){
        sethour('plus');
        return false;
      }
      if(charCode == 40){
        sethour('minus');
        return false;
      }
      */
        if(charCode==13){
          $('#myModal').modal('hide');
          complete();
          return false;
      }
      if(charCode >= 48 && charCode <= 57 || charCode==9){
        var b = String.fromCharCode(charCode);
        if(h.substr(-1, 1)){
          var sub = h.substr(-1, 1);
          var temp= sub+""+b;
          if(parseInt(temp)>23){
            document.getElementById(''+stt2).value="0";
          }else{
            document.getElementById(''+stt2).value=sub;
          }
        }else{
          document.getElementById(''+stt2).value="0";
        }
        return true;
      }else{
        return false;
      }
 
    }else if(stt=="minute"){
      if(stt2=="minute"){
        var m = $('input[name=minute]').val();
      }else{
        var m = $('input[name=min_end]').val();
      }    
      if(event.ctrlKey || event.altKey){
        return false;
      }
      /*if(charCode == 9){
        var range = document.getElementById('number_songs').value.length;
        document.getElementById('number_songs').focus();
        document.getElementById('number_songs').setSelectionRange(0,range);
        return false;
      }
      For FireFox
      if(charCode == 38){
        setmin('plus');
        return false;
      }
      if(charCode == 40){
        setmin('minus');
        return false;
      }
      */
      if(charCode==13){
          $('#myModal').modal('hide');
          complete();
          return false;
      }
      if(charCode >= 48 && charCode <= 57){
        var b = String.fromCharCode(charCode);
        if(m.substr(-1, 1)){
          var sub = m.substr(-1, 1);
          var temp= sub+""+b;
          if(parseInt(temp)>59){
            document.getElementById(''+stt2).value="0";
          }else{
            document.getElementById(''+stt2).value=sub;
          }
        }else{
          document.getElementById(''+stt2).value="0";
        }
        return true;
      }else{
        return false;
      }
    }else if(stt == "song"){
      var choose_text =document.getElementById('choose_text').value;
      var songs = document.getElementById('all_songs').value;
      var songs = parseInt(songs);
      var s = document.getElementById('number_songs').value;
      if(event.ctrlKey || event.altKey){
        return false;
      }
      /*if(charCode == 9){
        var range = document.getElementById('hour').value.length;
        document.getElementById('hour').focus();
        document.getElementById('hour').setSelectionRange(0,range);
        return false;
      }
       For FireFox
      if(charCode == 38){
        setnumbersong('plus',songs);
        return false;
      }
      if(charCode == 40){
        setnumbersong('minus',songs);
        return false;
      }
      */
      if(charCode==13){
        $('#myModal').modal('hide');
        complete();
        return false;
      }
      if(choose_text==0){
        return false;
      }else{
        if(charCode >= 48 && charCode <= 57){
          var b = String.fromCharCode(charCode);
          if(s.substr(-1, 1)){
            var sub = s.substr(-1, 1);
            var temp= sub+""+b;
            
            if(parseInt(temp)>songs){
              if(checkonly(songs)){
                if(b >= songs){
                  document.getElementById('number_songs').value="0"+songs;  
                  return false;
                }else if(b==0){
                  document.getElementById('number_songs').value="01";  
                  return false;
                }else{
                  document.getElementById('number_songs').value="0";  
                  return true;
                }   
              }else{
                if(b == 0){
                  document.getElementById('number_songs').value="01";
                  return false;
                }else{
                  document.getElementById('number_songs').value="0";
                  return true;
                }
              } 
            }else if(parseInt(temp)==0){
              document.getElementById('number_songs').value="01";
              return false;
            }else{
              document.getElementById('number_songs').value=sub;
            }
          }
          return true;
        }else{
          return false;
        }
      }
    }
  }
  function setnumbersong(stt,number){
    var s = document.getElementById('number_songs').value;
    if(stt == "plus"){
        var s = parseInt(s) +1;
        if(s<=number){
          if(checkonly(s)){
            document.getElementById('number_songs').value="0"+s;
          }else{
            document.getElementById('number_songs').value=s;
          }
        }else{
          document.getElementById('number_songs').value="01"; 
        }
    }else if(stt =="minus"){
        var s = parseInt(s) -1;
        if(s>=1){
          if(checkonly(s)){
            document.getElementById('number_songs').value="0"+s;
          }else{
            document.getElementById('number_songs').value=s;
          }
        }else{
          if(checkonly(number)){
            document.getElementById('number_songs').value="0"+number; 
          }else{
            document.getElementById('number_songs').value=number; 
          }      
        }
    }
  }
  function getsongs(){
    var id = get_firstchart();
    var songs =document.getElementById('songs'+id).value;
    return songs;
  }
  function get_firstchart(){
    for (var i = 1; i <= 5; i++) {
      if(document.getElementById('chart'+i)){
        var chart = document.getElementById('chart'+i).value;
        if(chart == 1){
          return i;
        }
      }
    };
  }
  function the_end(){
    var id = get_firstchart();
    document.getElementById('delete'+id).style.display="block";
    document.getElementById('myonoffswitch'+id).disabled=false; 
    document.getElementById('text_start'+id).disabled=false;//end time
    document.getElementById('songs'+id).disabled=false; 
    socket.emit('theend',id);
  }
  socket.on('theend',function(id){   
    var number_active=0;
    for (var i = 1; i <= 5; i++) {
      if(document.getElementById('chart'+i)){
        var act = document.getElementById('myonoffswitch'+i).checked;//acti  (0)
        if(!act){
          var t = document.getElementById('chart'+i).value;
          if(t>1){
            number_active=t;
            break;
          }
        }   
      }
    };
    if(number_active > 1){
      //set time
      settime(id,"theend");
      arrange_theend(id);      
    }else{
      document.getElementById('choose_action').value=0;
      clock.stop();
      var start;
      for (var i = 1; i <= 5; i++) {
        if(document.getElementById('chart'+i)){
          var temp = document.getElementById('chart'+i).value;
          if(temp==1){
            var start = document.getElementById('text_start'+i).value;
             break;
          }  
        }
      };

      var start = estimate_time(start);
      document.getElementById('choose_action').value=1;
      clock.setTime(start);
      clock.start();
    }
    
  });
  function arrange_theend(id){
    var idtime = document.getElementById('idtime'+id).value;
    var start = document.getElementById('text_start'+id).value;
    var songs = document.getElementById('songs'+id).value;
    var choice = $('#choice'+id).val();
    var end = $('#text_end'+id).val();
    var j= 0;
    var chart_min=0;
    number_add = 5;
    for (var i = 1; i <= number_add; i++) {
      if(i!=id){ 
        if(document.getElementById('myonoffswitch'+i)){
          var act = document.getElementById('myonoffswitch'+i).checked;
          if(!act){
            var chart= document.getElementById('chart'+i).value;
            if(chart > chart_min){
              chart_min=chart;
              j=i;
            }   
          }  
        }    
      }
    }
    var chart = document.getElementById('chart'+j).value;
    //arrange chart
    for (var i = 1; i <= 5; i++) {
        if(document.getElementById('myonoffswitch'+i)){
          var active=document.getElementById('myonoffswitch'+i).checked;        
          if(!active){
            var chart_temp = document.getElementById('chart'+i).value;
            document.getElementById('chart'+i).value=chart_temp-1;
          }
        }
      };  
    $.ajax({
        url: "{{Asset('/changetime')}}",
        type: 'post',
        data: {part:id,chart:chart_min,_token: token},
        success: function(data){}    
      },"json");
    setTimeout(function(){
      $("#part"+id).remove();
      $("#part"+j).after('<div id="part'+id+'"><li class="list-group-item my_item"><input type="hidden" id="choice'+id+'" value="'+choice+'"><input type="hidden" id="number_text'+id+'" value="'+id+'"><input type="hidden" id="idtime'+id+'" value="'+idtime+'"><div id="show_time"><input type="hidden" id="chart'+id+'" value="'+chart+'"><input type="text" class="text_class" id="text_start'+id+'" placeholder="00:00" value="'+start+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg @endif" onclick="setcurrent('+id+')"> &raquo; <div id="change_area"><input type="text" class="text_class" id="text_end'+id+'" placeholder="00:00" value="'+end+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" @endif onclick="setcurrent('+id+')"><div id="for_song'+id+'"><input type="text" class="text_class_songs" id="songs'+id+'" placeholder="00:00" value="'+songs+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" @endif onclick="setcurrent('+id+')"><label for="songs'+id+'"><span class="character">Songs</span></label></div><span id="limit'+id+'" class="limit_character">No Limit</span></div></div>@if(Session::has("admin"))<div class="del" id="delete'+id+'" onclick="delete_time('+id+')"><img src="'+img_del+'"></div>@endif<div class="onoffswitch my_onoffswitch"><input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch'+id+'"  @if(Session::has("admin")) onclick=changestatus('+id+') @else disabled @endif><label class="onoffswitch-label" for="myonoffswitch'+id+'" ><span class="onoffswitch-inner" ></span><span class="onoffswitch-switch"></span></label></div></li></div>');                          
      display_choice(id,choice);
      }, 300);
  }

                  var clock;
                    clock = $('.thame').FlipClock({
                        clockFace: 'HourCounter',
                        autoStart: false,
                        callbacks: {
                          stop: function() {
                            var choose = document.getElementById('choose_action').value;
                            if(choose==1){
                              disabled();
                              var id = get_firstchart();
                              var choice = $('#choice'+id).val();
                              var content = $('.Khai_user').html();
                              if(content=="user management"){admin=2;}
                      
                              if(choice == 0 ){
                                //end time
                                if(admin==2){
                                  songs_playing=499;
                                  myFunction1(2);
                                }
                              }else if(choice == 1){
                                //songs
                                if(admin==2){
                                  songs_playing= getsongs();
                                  alert(songs_playing);
                                  songs_playing-=1;
                                  myFunction1(2);
                               } 
                              }else{
                                //no limit
                                if(admin==2){
                                  songs_playing=799;
                                  myFunction1(2);
                                }
                              }

                              /*songs_playing= getsongs();
                              disabled();
                              var content = $('.Khai_user').html();
                              if(content=="user management"){admin=2;}
                              if(admin==2){
                                songs_playing-=1;
                                myFunction1(2);
                              }*/
                              //disabled();
                             // alert(choice);
                              //the_end();
                              
                            }

                          }
                        }
                    });
                    clock.setCountdown(true);
                    function flipclock(){
                      $.ajax({
                        url : "{{Asset('/time')}}",
                        datatype:"json",
                        type : "post",
                        data : {_token: token},
                        success : function (result){  
                            set_settime(result);                     
                        } 
                      }); 
                    };
                     var flip_clock=flipclock();
                     var img_del="{{Asset('/assets/img/del.png')}}";
                     function set_settime(json){
                      var obj = JSON && JSON.parse(json) || $.parseJSON(json);
                      var length=Object.keys(obj.alltime).length; 
                      if(length==0){
                        clock.setTime(0);
                        document.getElementById('choose_action').value=0;
                        clock.stop();
                        document.getElementById('number_add').value=0;
                        document.getElementById('separation').value=1;
                        document.getElementById('after').value=0;
                        
                      }else{                             
                        if(document.getElementById('linh_add_btn')){
                          if(length > 4 ){
                            document.getElementById('linh_add_btn').style.display="none";
                          }else{
                            document.getElementById('linh_add_btn').style.display="inline";
                          }
                        }
                        $("#linh_ol").append('<div id="part0"></div>');
                        for (var i = 1; i <= length; i++) {
                          j=i-1;
                          var active = obj.alltime[j].active;
                          var start = obj.alltime[j].time_start.substr(0,5);
                          var i = obj.alltime[j].part;
                          var choice = obj.alltime[j].choice;
                          var songs = obj.alltime[j].songs;
                          var end = obj.alltime[j].time_end;
                          if(checkonly(songs)){
                            songs = "0"+songs;
                          }
                          if(active == 1){ 
                            $("#linh_ol").append('<div id="part'+i+'"><li class="list-group-item my_item"><input type="hidden" id="choice'+i+'" value="'+choice+'"><input type="hidden" id="number_text'+i+'" value="'+i+'"><input type="hidden" id="idtime'+i+'" value="'+obj.alltime[j].id_time+'"><div id="show_time"><input type="hidden" id="chart'+i+'" value="'+obj.alltime[j].chart+'"><input type="text" class="text_class" id="text_start'+i+'" placeholder="00:00" value="'+start+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg @endif" onclick="setcurrent('+i+')"> &raquo; <div id="change_area"><input type="text" class="text_class text_end" id="text_end'+i+'" placeholder="00:00" value="'+end+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" @endif onclick="setcurrent('+i+')"><div id="for_song'+i+'"><input type="text" class="text_class_songs" id="songs'+i+'" placeholder="00:00" value="'+songs+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" @endif onclick="setcurrent('+i+')"><label for="songs'+i+'"><span class="character">Songs</span></label></div><span id="limit'+i+'" class="limit_character">No Limit</span></div></div>@if(Session::has("admin"))<div class="del" id="delete'+i+'" onclick="delete_time('+i+')"><img src="'+img_del+'"></div>@endif<div class="onoffswitch my_onoffswitch"><input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch'+i+'" @if(Session::has("admin"))onclick=changestatus('+i+') @else disabled @endif><label class="onoffswitch-label" for="myonoffswitch'+i+'" ><span class="onoffswitch-inner" ></span><span class="onoffswitch-switch"></span></label></div></li></div>');
                          }else{    
                            $("#linh_ol").append('<div id="part'+i+'"><li class="list-group-item my_item"><input type="hidden" id="choice'+i+'" value="'+choice+'"><input type="hidden" id="number_text'+i+'" value="'+i+'"><input type="hidden" id="idtime'+i+'" value="'+obj.alltime[j].id_time+'"><div id="show_time"><input type="hidden" id="chart'+i+'" value="'+obj.alltime[j].chart+'"><input type="text" class="text_class" id="text_start'+i+'" placeholder="00:00" value="'+start+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg @endif" onclick="setcurrent('+i+')"> &raquo; <div id="change_area"><input type="text" class="text_class text_end" id="text_end'+i+'" placeholder="00:00" value="'+end+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" @endif onclick="setcurrent('+i+')"><div id="for_song'+i+'"><input type="text" class="text_class_songs" id="songs'+i+'" placeholder="00:00" value="'+songs+'" readonly @if(Session::has("admin")) class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" @endif onclick="setcurrent('+i+')"><label for="songs'+i+'"><span class="character">Songs</span></label></div><span id="limit'+i+'" class="limit_character">No Limit</span></div></div>@if(Session::has("admin"))<div class="del" id="delete'+i+'" onclick="delete_time('+i+')"><img src="'+img_del+'"></div>@endif<div class="onoffswitch my_onoffswitch"><input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch'+i+'" checked="" @if(Session::has("admin")) onclick=changestatus('+i+') @else disabled @endif><label class="onoffswitch-label" for="myonoffswitch'+i+'" ><span class="onoffswitch-inner" ></span><span class="onoffswitch-switch"></span></label></div></li></div>');
                          }
                      
                          display_choice(i,choice);
                          i=j+1;
                        }   
                        document.getElementById('number_add').value=length;
                        var k =-1;
                        var j=0;
                        if(obj.alltime[length-1].active==1){
                          k=length;
                          j=length+1;
                        }else{
                            for (var i = 0; i < length ; i++) {
                            if(obj.alltime[i].active==0){
                              j=i+1;
                              k=i;
                              break;
                            }
                          };
                        }
                        
                        document.getElementById('separation').value=j;

                        var now=get_time_now();
                        var array=[];
                        var day = 23*60*60 + 59*60 + 59;
                        for (var i = 1; i <= length; i++) {
                          var start = obj.alltime[i-1].time_start.substr(0,5);
                          var hour = parseInt(start.substr(0, 2));
                          var min = parseInt(start.substr(-2, 2));
                          var minus= hour * 60 * 60 + min * 60 - now;
                          if(minus< 0){
                            array[i]=day+minus;
                          }else{
                            array[i]=minus;
                          }                         
                        };
                        if(k >= 1){
                          var choose =  document.getElementById('choose_action').value;
                          document.getElementById('choose_action').value=0;
                          clock.stop();
                          clock.setTime(array[1]);
                          clock.start();
                          document.getElementById('choose_action').value=1;
                        }else{
                          document.getElementById('choose_action').value=0;
                          clock.setTime(0);
                          clock.stop();  
                        }

                      }
                    }
        
    function get_time_now () {
      var tmp = null;
      $.ajax({
          async: false,
          type: "POST",
          global: false,
          url: "{{Asset('/getcurrenttime')}}",
          data: {_token: token},
          success: function (data) {
            var hour = parseInt(data.substr(0, 2));
            var min = parseInt(data.substr(3, 2))
            var sec = parseInt(data.substr(-2, 2));
              tmp = (hour*60*60) + (min*60) + sec;
          }
      });
      return tmp;
    };
    function disabled(){
     var id = get_firstchart();
     if(document.getElementById('delete'+id)){
      document.getElementById('delete'+id).style.display="none";
     } 
     document.getElementById('myonoffswitch'+id).disabled=true;
     document.getElementById('text_start'+id).disabled=true;
     document.getElementById('songs'+id).disabled=true; 
    }
    function isPlayed(duration){
      var id = get_firstchart();
      var end= document.getElementById('text_end'+id).value;
      var rest_time = estimate_time(end);
      if(duration <= rest_time){
        return true;
      }else{
        return false;
      }
      
    }
</script>

<!DOCTYPE html>
<html lang="en">

<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Music Chart - First Project</title>
    <script language="javascript" src="http://code.jquery.com/jquery-2.0.0.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/blog-post.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style1.css') }}" media="screen">
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 80%;
      margin: auto;
  }
  </style>
            
</head>

<body>
  

<input type="hidden" name="_token" value="{{ csrf_token() }}">
<!-- Page Content -->
<div class="container">
  <a id="icback" href="{{ url('/') }}"><img src="http://s5.picofile.com/file/8132929226/MB_0006_back.png" width="35" height="35"></a><span><h1>User management</h1></span>
    <div class='row-sm-12'>
      <form>
        <table class="table table-striped table-hover acc">
          <thead>
            <tr>
              <th>Username</th>
              <th>Email</th>
              <th>Vote</th>
              <th>Level</th>
              <th>Active</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($users as $user)
            <tr id="row{{$user->id}}">
              <td>{{$user->name}}</td>
              <td>{{$user->email}}</td>
              <td>{{$user->votes}}</td>
              <td>
                @if($user->level==1)
                  Admin
                @endif
                @if($user->level==2)
                  User
                @endif
                @if($user->level==3)
                  Slaver
                @endif
              </td>
              <td id="check{{$user->id}}">{{$user->check}}</td>
              <td>
                @if($user->check!='no')
                  @if($user->level!=1)
                    @if($user->check=='block')
                      <a id="{{$user->id}}" class="btn mybtn">Active</a>
                    @else
                      <a id="{{$user->id}}" class="btn mybtn">Block</a>
                      @endif
                  @endif
                @endif
              </td>
            </tr>
          </tbody>
            @endforeach
        </table>
      </form>
    </div>
</div>
<script language="javascript">
  $(document).ready(function(){
    $("body").on('click','.acc a',function(){
      var token=$('input[name=_token]').val();
      var ids = $(this).attr('id');
      $.ajax({
        url : "{{asset('blockaccount')}}",
        type : "post",
        dateType:"",
        data : {
           _token: token,ids: ids,
        },
        success : function (result){
          $("#check"+ids).html(result);
          if($("#check"+ids).text()=='block'){
            $(".acc a").filter("#"+ids).slideUp('slow');
            $(".acc a").filter("#"+ids).slideDown("slow");
            $(".acc a").filter("#"+ids).text('Active'); 
          }
          else{
            $(".acc a").filter("#"+ids).slideUp('slow');
            $(".acc a").filter("#"+ids).slideDown("slow");
            $(".acc a").filter("#"+ids).text('Block');
          }
        }
      });
    });
  });
</script>
</body>
